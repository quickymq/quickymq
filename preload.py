# -*-coding:utf-8-*-

import base64
import re
import traceback
import requests
import time
import os
# import logging
import small_fun


def show_logo():
    print('\n\n======================================================')
    print('||=======欢迎使用《BNU体育馆-羽毛球馆》预约程序=====||')
    print('======================================================\n\n')


def show_notice(content):
    notice = re.findall('\*\*(.*?)\*\*', content)[0]
    print(notice)


def show_list(content):
    li = re.findall('!!(.*?)!!', content)[0]
    d = li.replace(';', '\n')
    b = re.split('\n', d)
    for i in b:
        c = i.split(',')
        for j in c:
            print('%-13s' % j, end='')
            time.sleep(0.01)
        print('\n')


def check_status(content):
    statues = re.findall('\$\$(.*?)\$\$', content)[0]
    if statues == 'on':
        print('端口预约许可检查通过\n\n')
        # logging.info("端口预约许可检查通过")
        return 1
    else:
        print('由于近期大量测试导致系统崩溃，近几天将关闭预约服务，请过几天再试')
        # logging.warning("由于近期大量测试导致系统崩溃，近几天将关闭预约服务，请过几天再试")
        return 0


def block_check(content, id):
    blockid = re.findall('@@(.*?)@@', content)[0]
    if id in blockid.split('&'):
        print('由于您的违规行为，您无法访问')
        # logging.warning("由于您的违规行为，您无法访问")
        return 0
    else:
        return 1
    

def version_check(nowversion,content):
    vinfor = re.findall('\^\^(.*?)\^\^',content)
    new_v = eval(vinfor[0])
    if nowversion < new_v:
        print('【发现新版本】当前版本为：v%s,最新版本为v%s'%(nowversion,new_v))
        # logging.info('【发现新版本】当前版本为：v%s,最新版本为v%s'%(nowversion,new_v))
        print(vinfor[1])
    else:
        print('当前为最新版本')
        
def creatfile(file_name):
    os.system ("cls")
    try:
        file_path = r'C:\Users\Administrator\BNUbadminton\\' + file_name
        if os.path.exists(file_path) == False:
            if os.path.exists(r'C:\Users\Administrator\BNUbadminton') == False:
                os.makedirs(r'C:\Users\Administrator\BNUbadminton')
            with open (file_path,'w') as f:
                f.write('')
    except:
        error_infor0 = traceback.format_exc()
        print(error_infor0)
        print('【ERROR!】第一次运行该软件需以管理员身份运行，当前用户无法创建必要文件; 或您需手动在上述错误的路径处创建文件')
        # logging.error("第一次运行该软件需以管理员身份运行，当前用户无法创建必要文件; 或您需手动在上述错误的路径处创建文件")
    

# 主函数
def preload_main(now_v):
    # logging.basicConfig(filename=r'C:\Users\Administrator\BNUbadminton\app.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')  # ,filemode='w'
    creatfile('sk.dat')
    # 初始化
    show_logo()
    checkcode = input("请输入软件授权码(https://quickymq.hikerwatermelon.tk/ 可获取)：")
    if checkcode == base64.b64decode('ISEp').decode('ascii') or checkcode == base64.b64decode('SSBMb3ZlIEJhZG1pbnRvbg==').decode('ascii'):
        # logging.info("软件授权码输入正确")
        try:
            # r = requests.get(
                # base64.b64decode('aHR0cHM6Ly9naXRlZS5jb20vYmFybG93bi90ZW1wX3Rlc3QvcmF3L21hc3Rlci8xLmRhdA==').decode(
                    # 'ascii'))
            r = base64.b64decode(requests.get(base64.b64decode('aHR0cHM6Ly9naXRlZS5jb20vYmFybG93bi9xYmFkbWluL3Jhdy9tYXN0ZXIvY2hlY2s=').decode('ascii')).text).decode('utf-8')
            r1 = re.findall(r'~~~~~~~~~~begin[\s\S]*?end~~~~', r)[0]
            version_check(now_v,r1)
            # Status check
            if check_status(r1):
                print('请最大化窗口显示 if need')
                show_list(r1)
                show_notice(r1)
                print("\n\n")
                return True
        except:
            error_infor = traceback.format_exc()
            a = r"(host='gitee.com', port=443): Max retries exceeded with url: /barlown/temp_test/blob/master/1.dat"
            if a in error_infor:
                error_infor = error_infor.replace(a, '{host error}')
                print(error_infor)
            else:
                print('traceback.format_exc():\n%s' % error_infor)
            print('网络异常，请检查您是否关闭VPN或是否已经连接校园网，请检查完毕后重试，如果还有问题，请检查运行错误日志\n 错误日志在上面↑↑')
            # logging.info("网络异常，请检查您是否关闭VPN或是否已经连接校园网，请检查完毕后重试")
            print('错误日志输出完毕，请自行检查，不会的话请联系软件开发者，估计他也很忙没空管这个，还是找度娘吧')
            return False
        # input('按任意键继续')
    else:
        input('授权码输入错误，88了您nei~')
        # logging.info("授权码输入错误")
        return False



if __name__ == '__main__':
    preload_main(3.1)