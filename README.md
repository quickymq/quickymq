# Readme

本文件是QuickYMQ使用说明，包含Termux安装及其他必要配置说明。

[TOC]

## 安装说明 

### 初始化配置 详细介绍版

1. 下载Termux, termux-API: 

   [Termux下载地址](https://f-droid.org/repo/com.termux_118.apk)，[Termux-API](https://f-droid.org/repo/com.termux.api_51.apk)

2. 设置Termux ,Termux-API的权限。
   * Termux : 存储和关闭省电策略
   * Termux-API: 给短信相关的权限。 系统设置-应用管理-权限设置。备注：Termux-API无程序图标，仅仅是插件而已。

3. 入门配置Termux

   * 更换Termux文件下载源： 

     * 【推荐】在较新版的 Termux 中，官方提供了图形界面（TUI）来半自动替换镜像，推荐使用该种方式以规避其他风险。 在 Termux 中执行如下命令

       ```
       termux-change-repo
       ```

     ​		在图形界面引导下，使用自带方向键可上下移动。
     ​		第一步使用空格选择需要更换的仓库，之后在第二步选择 TUNA/BFSU 镜像源。确认无		误后回车，镜像源会自动完成更换。

     * 或者命令手动修改

       ```
       sed -i 's@^\(deb.*stable main\)$@#\1\ndeb https://mirrors.tuna.tsinghua.edu.cn/termux/apt/termux-main stable main@' $PREFIX/etc/apt/sources.list
       apt update && apt upgrade
       ```

   * 更新安装工具

     ```
     pkg upgrade
     ```

   * 安装vim nodejs python git及配套插件

     vim: 

     ```linux
     pkg install vim
     ```

     nodejs

     ```
     pkg install nodejs
     ```

     git 

     ```
     pkg install git
     git clone https://gitlab.com/quickymq/quickymq.git
     ```

     python

     ```
     pkg install python
     # 配置pypi源
     pip install pip -U
     pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
     # 依赖python项
     pip install wheel
     pip install setuptools --upgrade
     # 安装QuickYMQ依赖项
     cd quickymq
     pip install -r requirements.txt
     ```

4. 配置QuickYMQ 文件

   ```
   cd
   cd quickymq
   vim run*
   # 键盘输入 i ,进入编辑模式，填入学号、密码、场地信息等。编辑完成后，点击esc,退出编辑模式。键盘输入 :wq ,保存退出。
   python run* 即可测试本程序是否正常执行。
   ```


### 初始化配置懒蛋版

1. 下载Termux, termux-API: 

   [Termux下载地址](https://f-droid.org/repo/com.termux_118.apk)，[Termux-API](https://f-droid.org/repo/com.termux.api_51.apk)

2. 设置Termux ,Termux-API的权限。

   * Termux : 存储和关闭省电策略
   * Termux-API: 给短信相关的权限。 系统设置-应用管理-权限设置。备注：Termux-API无程序图标，仅仅是插件而已。

3. 把下面的代码粘贴到新安装的Termux中【小白懒蛋闭眼操作向】，让输入东西的时候一路点回车就行。

   ```
   sed -i 's@^\(deb.*stable main\)$@#\1\ndeb https://mirrors.tuna.tsinghua.edu.cn/termux/apt/termux-main stable main@' $PREFIX/etc/apt/sources.list;apt update && apt upgrade;pkg install vim;pkg install nodejs;pkg install git;git clone https://gitlab.com/quickymq/quickymq.git;pkg install python;pip install pip -U;pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple;pip install wheel;pip install setuptools --upgrade;cd quickymq;pip install -r requirements.txt
   ```

4. 配置QuickYMQ 文件

   ```
   vim run*
   # 键盘输入 i ,进入编辑模式，填入学号、密码、场地信息等。编辑完成后，点击esc,退出编辑模式。键盘输入 :wq ,保存退出。
   python run* 即可测试本程序是否正常执行。
   ```

## QuickYMQ程序使用说明

```
mybook = 啦啦啦_Badminton_intranet_apply('202******','mima',['5925','5927'],date,'07:30:00','填入Server酱的Key',0,0)
```

`'202******'` 是学号，

`'mima'` 是密码

`['5925','5927']`场地编号

以上修改的内容只修改数字或者英文字母，其他符号不能动。

```
例如
mybook = 请替换这里的字_Badminton_intranet_apply('2029655286148','aiyoubucuoyou',['5925','5927'],date,'07:30:00','SCT654684634njkhj46khjk1j3kg3hqIjFDVajhkhYKH',0,0)
```



## 高阶定时操作使用说明

目的： 实现每周周x预约球馆

1. 安装定时软件

   ```
   pkg install cronie
   ```

2. 配置日程管理事项

   定时任务启动 `crond` 并部署任务。[crond帮助文件](https://linuxhandbook.com/start-stop-restart-cron/)   以下仅供参考

   ``` 
   # 详细步骤如下：
   # 计划程序建立方法
   # 启动crontab计划任务
   service crond start
   # 建立计划任务
   crontab -e
   # 编辑计划任务（根据下面的自行修改）
   29 7 * * 2,3 cd ~/quickymq;python run*.py>>~/quickymq/book.log 2>&1
   # 分钟 小时 每周的周二周三 （执行index.py程序）运行日志在quickymq/book.log中
   # 查看当前计划任务列表
   crontab -l
   ```

3. 设置cron开机启动

   参看下面文章 

   [termux开机自启sshd crond vnc编辑启动项_termux自启动](https://blog.csdn.net/nongcunqq/article/details/119105394)

   

## 附录

![](https://s3.bmp.ovh/imgs/2023/02/27/05a410b11c9f1d00.png)





